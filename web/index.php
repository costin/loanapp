<?php

use Costin\Restapi\Common\Application;
use Costin\Restapi\Common\Container;
use Costin\Restapi\Common\HttpRequest;
use Costin\Restapi\Db\Db;
use Costin\Restapi\Rest\Router;
use Costin\Restapi\Rest\Translator;

header('Content-Type:application/json');

require_once dirname(__FILE__) . '/../vendor/autoload.php';
require_once dirname(__FILE__) . '/../app/config/config.php';

$container = new Container();
$container
    ->add('httpRequest', new HttpRequest())
    ->add('translator', new Translator($container->get('httpRequest')))
    ->add('router', new Router($container->get('translator')))
    ->add(
        'db',
        Db::getInstance($config['host'], $config['database'], $config['username'], $config['password'])
    );

$application = new Application($container);
$application->run();
