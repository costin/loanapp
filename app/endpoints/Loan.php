<?php

namespace App\Endpoints;

use App\Services\LoanService;
use Costin\Restapi\Rest\Endpoint;

class Loan extends Endpoint
{
    public function get()
    {
        $loanService = LoanService::getInstance($this->getContainer()->get('db'));

        return $this->formatResponse($loanService->getLoans($this->getParameter()));
    }

    public function post()
    {
        $loanService = LoanService::getInstance($this->getContainer()->get('db'));

        return $this->formatResponse($loanService->addLoan($this->getRequest()));
    }
}
