<?php

namespace App\Endpoints;

use App\Services\LoanPaymentsService;
use Costin\Restapi\Rest\Endpoint;

class LoanPayments extends Endpoint
{
    public function get()
    {
        $loanPaymentsService = LoanPaymentsService::getInstance($this->getContainer()->get('db'));

        return $this->formatResponse(
            $loanPaymentsService->calculateLoanPayments(
                $this->getParameter(),
                $this->request
            )
        );
    }
}
