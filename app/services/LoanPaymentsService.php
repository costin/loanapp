<?php

namespace App\Services;

use Costin\Restapi\Db\Db;

class LoanPaymentsService
{
    public $db = null;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    public static function getInstance(Db $db)
    {
        return new self($db);
    }

    public function calculateLoanPayments($parameter, $request)
    {
        $loan = $this->getLoan($parameter);

        if (empty($loan)) {
            return 'Cannot find requested loan';
        }

        $returnPayments = [];

        $i = 1;
        $date = date('Y-m-d');
        $beginningBalance = $loan['amount'];
        $payment = $this->pmt($loan['interest'], $loan['duration'], $loan['amount']);

        $interest = round($beginningBalance * ($loan['interest'] / 12), 2);
        $principal = $payment - $interest;

        $cumulativePrincipal = 0;
        $cumulativeInterest = 0;
        $endingBalance = round($beginningBalance - $principal, 2);

        $returnPayments[] = [
            'Period' => date('Y-m-d'),
            'Beginning Balance' => $beginningBalance,
            'Payment' => $payment,
            'Principal' => $principal,
            'Interest' => $interest,
            'Cumulative Principal' => $cumulativePrincipal,
            'Cumulative Interest' => $cumulativeInterest,
            'Ending Balance' => $endingBalance
        ];

        do {
            $date = date('Y-m-d', strtotime(date('Y-m-d', strtotime($date)) . ' +1 month'));
            $beginningBalance = $endingBalance;

            $interest = round($beginningBalance * ($loan['interest'] / 12), 2);
            $principal = $payment - $interest;

            $cumulativePrincipal += $principal;
            $cumulativeInterest += $interest;

            $endingBalance = round($beginningBalance - $principal, 2);

            $returnPayments[] = [
                'Period' => $date,
                'Beginning Balance' => $beginningBalance,
                'Payment' => $payment,
                'Principal' => $principal,
                'Interest' => $interest,
                'Cumulative Principal' => $cumulativePrincipal,
                'Cumulative Interest' => $cumulativeInterest,
                'Ending Balance' => $endingBalance
            ];

            $i++;
        } while ($i < $loan['duration']);


        return $returnPayments;
    }

    private function getLoan($id)
    {
        $sql = 'SELECT * FROM loan WHERE id = :id';
        $args[':id'] = $id;

        return $this->db->execute($sql, $args)->fetchAll()[0];
    }

    private function pmt($interest, $duration, $amount)
    {
        $interest = $interest / 12;
        return number_format(
            $interest * -$amount * pow((1 + $interest), $duration) / (1 - pow((1 + $interest), $duration)),
            2
        );
    }
}
