<?php

namespace App\Services;

use Costin\Restapi\Db\Db;

class LoanService
{
    public $db = null;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    public static function getInstance(Db $db)
    {
        return new self($db);
    }

    public function getLoans($request)
    {
        $args = [];
        $where = '';

        if (!empty($request)) {
            $args[':id'] = $request;
            $where = 'WHERE id = :id';
        }

        $sql = 'SELECT * FROM loan ' . $where;

        return $this->db->execute($sql, $args)->fetchAll();
    }

    public function addLoan($request)
    {
        if (!isset($request['amount']) || !isset($request['interest']) || !isset($request['duration'])) {
            return('Please set all the properties for the request!');
        }

        $args = [
            ':amount' => $request['amount'],
            ':interest' => $request['interest'],
            ':duration' => $request['duration']
        ];

        $sql = 'INSERT INTO loan (amount, interest, duration) VALUES (:amount, :interest, :duration)';

        return $this->db->execute($sql, $args)->errorCode() === '00000';
    }
}
